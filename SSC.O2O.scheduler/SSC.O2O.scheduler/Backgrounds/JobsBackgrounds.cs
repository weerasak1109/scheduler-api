﻿using Hangfire;
using SSC.O2O.Scheduler.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SSC.O2O.Scheduler.Backgrounds
{
    /// <summary>
    /// Jobs backgrounds
    /// </summary>
    public class JobsBackgrounds
    {
        /// <summary>
        /// Run
        /// </summary>
        /// <returns></returns>
        public static void Run()
        {
            RecurringJob.RemoveIfExists(nameof(PayVoidService));
            RecurringJob.RemoveIfExists(nameof(OrderReportService));

            //RecurringJob.AddOrUpdate<PayVoidService>(nameof(PayVoidService), job => job.Run(), Cron.Daily(19, 45), TimeZoneInfo.Local);
            RecurringJob.AddOrUpdate<OrderReportService>(nameof(OrderReportService), job => job.Run(), Cron.Daily(19, 40), TimeZoneInfo.Local);
        }
    }
}
