﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SSC.O2O.Scheduler.Helpers
{
    /// <summary>
    /// App settings
    /// </summary>
    public class AppSettings
    {
        /// <summary>
        /// Route prefix
        /// </summary>
        public string RoutePrefix { get; set; }
    }
}
