﻿using Microsoft.Extensions.Localization;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using MongoDB.Bson;
using MongoDB.Driver;
using SSC.O2O.Scheduler.Entities.MongoDB;
using SSC.O2O.Scheduler.Helpers;
using SSC.O2O.Scheduler.Interfaces;
using SSC.O2O.Scheduler.Resources;
using SSC.O2O.Scheduler.Utilities;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;

namespace SSC.O2O.Scheduler.Services
{
    /// <summary>
    /// Gen Order report service
    /// </summary>
    public class OrderReportService : ISchedulerService
    {
        private readonly ConnectionStrings _connect;
        private readonly IStringLocalizer _localizer;
        private readonly ISysService _sys;
        private readonly ILogger _logger;
        private readonly IMongoCollection<t_online_payment> _tonlinepayment;
        private readonly IMongoCollection<t_online_order> _tonlineorder;
        private readonly IMongoCollection<t_online_tracking> _tonlinetracking;
        private readonly IMongoCollection<t_online_order_report> _tonlinereport;
        private readonly IMongoCollection<t_ct_qo> _tctqo;
        private readonly IMongoCollection<m_payment> _mpayment;
        

        /// <summary>
        /// Gen Order report service constructor
        /// </summary>
        /// <param name="logger">logger</param>
        /// <param name="connect">connect</param>
        /// <param name="factory">factory</param>
        public OrderReportService(ILogger<OrderReportService> logger,IOptions<ConnectionStrings> connect,IStringLocalizerFactory factory, ISysService sys)
        {
            try
            {
                _logger = logger;
                _connect = connect.Value;
                _sys = sys;

                var type = typeof(ErrorMessage);
                var assemblyName = new AssemblyName(type.GetTypeInfo().Assembly.FullName);
                _localizer = factory.Create(typeof(ErrorMessage).Name, assemblyName.Name);

                var client = new MongoClient(_connect.NoSqlContext);
                var database = client.GetDatabase(new MongoUrl(_connect.NoSqlContext).DatabaseName);
                _tonlinereport = database.GetCollection<t_online_order_report>(typeof(t_online_order_report).Name);
                _tonlinepayment = database.GetCollection<t_online_payment>(typeof(t_online_payment).Name);
                _tonlineorder = database.GetCollection<t_online_order>(typeof(t_online_order).Name);
                _tonlinetracking = database.GetCollection<t_online_tracking>(typeof(t_online_tracking).Name);
                _tctqo = database.GetCollection<t_ct_qo>(typeof(t_ct_qo).Name);
                _mpayment = database.GetCollection<m_payment>(typeof(m_payment).Name);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, string.Format("{0} -- {1}", nameof(OrderReportService), ex.Message));
            }
        }

        /// <summary>
        /// Generate online order report
        /// </summary>
        [DisplayName("Generate online order report")]
        public void Run()
        {
            try
            {
                #region Variable

                #endregion

                #region query
                var reports =
                from e in _tonlinereport.AsQueryable<t_online_order_report>()
                .OrderByDescending(o => o.edit_date)
                .Take(1)
                where e.inactive == false                
                select e;
                DateTime? date_last = null;
                if (reports != null && reports.Count() > 0)
                {
                    t_online_order_report report = reports.FirstOrDefault();
                    date_last = report.edit_date;
                }
                List<t_online_order> online_order = null;
                if (date_last == null) online_order = _tonlineorder.Find(f => f.last_status.order_status == ConstUtility.RECEIVE_ORDER).ToList();
                else online_order = _tonlineorder.Find(f => f.last_status.order_status_date >= date_last.Value.AddMinutes(-5) && f.last_status.order_status == ConstUtility.RECEIVE_ORDER).ToList();

                List<t_online_payment> online_payment = null;
                List<t_online_tracking> online_tracking = null;
                if (online_order != null)
                {
                    List<Int64> o_pay_id = online_order.GroupBy(g => g.o_pay_id).Select(s => s.Key).ToList();
                    online_payment = _tonlinepayment.Find(f => o_pay_id.Contains(f.o_pay_id)).ToList();
                    online_tracking = _tonlinetracking.Find(f => o_pay_id.Contains(f.o_pay_id)).ToList();
                }
                List<m_payment> fee_all = _mpayment.Find(f => f.inactive == false).ToList();
                #endregion

                if (online_order != null)
                {
                    List<t_online_order_report> reps_save = new List<t_online_order_report>();
                    //List<t_online_order_report> reps_edit = new List<t_online_order_report>();
                    foreach (var o in online_order)
                    {
                        t_online_payment pay = online_payment.Find(f => f.o_pay_id == o.o_pay_id);
                        m_payment fee = fee_all.Find(f => f.py_id == o.py_id);
                        if (pay == null || fee == null) continue;
                        int seq = 1;
                        foreach (var i in o.item_info)
                        {
                            #region check data update
                            var chk =
                            from e in _tonlinereport.AsQueryable<t_online_order_report>()
                            .Take(1)
                            where e.o_pay_id == o.o_pay_id &&
                            e.o_order_id == o.o_order_id &&
                            e.sc_doc_id == o.sc_doc_id &&
                            e.qo_doc_id == i.qo_doc_id &&
                            e.item_id == i.item_id &&
                            e.order_item_seq == seq
                            select e;                            

                            t_online_order_report rep = null;
                            if (chk != null && chk.Count() > 0)
                            {
                                continue;                              
                            }
                            else
                            {
                                rep = new t_online_order_report()
                                {
                                    _id = ObjectId.GenerateNewId(),
                                    o_report_id = _sys.GetIncrement(typeof(t_online_order_report).Name)                                    
                                };
                                rep.Create(0);
                            }

                            #endregion

                            #region Data all
                            rep.o_pay_id = pay.o_pay_id;
                            rep.o_pay_no = pay.o_pay_no;
                            rep.o_pay_date = pay.o_pay_date;
                            rep.o_order_id = o.o_order_id;
                            rep.o_order_no = o.o_order_no;
                            rep.o_order_date = o.o_order_date;
                            rep.order_date = pay.o_pay_date;
                            rep.ref_sc_no = o.ref_sc_no;

                            rep.sc_doc_id = o.sc_doc_id;
                            rep.sc_doc_type = o.sc_doc_type;
                            rep.sc_name = o.sc_doc_name;

                            rep.track_no = i.o_track_no;
                            rep.o_track_id = i.o_track_id;

                            rep.py_type = pay.py_name;
                            rep.py_id = pay.py_id;
                            rep.py_outlet_id = pay.py_outlet_id;
                            rep.py_outlet_code = pay.py_outlet_code;
                            rep.py_outlet_name = pay.py_outlet_name;

                            rep.order_item_seq = seq;
                            rep.qo_doc_id = i.qo_doc_id;
                            rep.item_id = i.item_id;
                            rep.item_name = i.item_name;
                            rep.cost_price = i.base_cost;
                            rep.qty = Convert.ToInt64(i.qty);
                            rep.total_cost = i.cost_show;
                            rep.sale_price = i.base_price;
                            rep.total_price = i.price_show;

                            rep.disc_promotion = pay.order_total_discount;
                            rep.lg_disc = pay.ship_total_discount;

                            rep.order_total_amt = pay.order_total_amt - pay.order_total_discount;
                            rep.total_amt = pay.total_amt;

                            rep.py_plateform_price = pay.platform_fee_amt;
                            rep.order_wlg = pay.total_amt - pay.platform_fee_amt;

                            rep.py_plateform_price_vat = (pay.platform_fee_amt * 7) / 107;
                            rep.wov_plateform_price = pay.platform_fee_amt - ((pay.platform_fee_amt * 7) / 107);

                            rep.lg_price = pay.ship_total_amt;
                            rep.shop_name = o.seller_outlet_name;
                            rep.seller_name = i.post_outlet_name;
                            rep.wsp_id = o.wsp_id;
                            rep.Edit(0);
                            #endregion

                            #region tracking status
                            if (online_tracking != null)
                            {
                                var trackings = online_tracking.Find(f => f.o_pay_id == o.o_pay_id && f.o_order_id == o.o_order_id && f.o_track_id == i.o_track_id);
                                if (trackings != null && trackings.log_info != null && trackings.log_info.Count > 0)
                                {
                                    var tracking = trackings.log_info.Last();
                                    if (tracking.log_name != null) rep.tracking_status = tracking.log_name;
                                }
                            }
                            #endregion

                            #region provider fee
                            if (fee != null)
                            {
                                if (fee.py_calc_type == ConstUtility.PY_CALC_PERCENT)
                                {
                                    rep.py_provider_price = (rep.total_amt * fee.new_provider_fee_rate) / 100;
                                }
                                else if (fee.py_calc_type == ConstUtility.PY_CALC_AMOUNT)
                                {
                                    rep.py_provider_price = fee.new_provider_fee_rate;
                                }
                            }
                            rep.py_provider_price_vat = (rep.py_provider_price * 7) / 107;
                            rep.wov_provider_price = rep.py_provider_price - rep.py_provider_price_vat;
                            rep.diff_py_fee = rep.wov_plateform_price - rep.wov_provider_price;
                            #endregion

                            #region lg
                            if (o.shipping_charge != null && o.shipping_charge.Count > 0)
                            {
                                var lg = o.shipping_charge.FirstOrDefault();
                                if (lg != null)
                                {
                                    rep.lg_provider_price = lg.provider_price;
                                    rep.lg_provider_price_vat = rep.lg_provider_price * 7 / 107;
                                    rep.wov_lg_provider_price = rep.lg_provider_price - rep.lg_provider_price_vat;

                                    rep.lg_plateform_price = lg.platform_price;
                                    rep.lg_plateform_price_vat = rep.lg_plateform_price * 7 / 107;
                                    rep.wov_lg_plateform_price = rep.lg_plateform_price - rep.lg_plateform_price_vat;

                                    rep.diff_lg_price = rep.wov_lg_plateform_price - rep.wov_lg_provider_price;
                                }
                            }
                            #endregion

                            rep.SetRound();
                            reps_save.Add(rep);
                                                        
                            seq++;
                        }
                    }

                    if (reps_save.Count > 0) _tonlinereport.InsertMany(reps_save);
                }                
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, string.Format("{0} -- {1}", nameof(OrderReportService), ex.Message));
            }
        }
    }
}
