﻿using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using MongoDB.Driver;
using SSC.O2O.Scheduler.Entities.MongoDB;
using SSC.O2O.Scheduler.Helpers;
using SSC.O2O.Scheduler.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SSC.O2O.Scheduler.Services
{
    /// <summary>
    /// System service
    /// </summary>
    public class SysService : ISysService
    {
        private readonly ConnectionStrings _connect;
        private readonly ILogger _logger;
        private readonly IMongoCollection<counters> _counters;

        /// <summary>
        /// System service constructor
        /// </summary>
        /// <param name="logger">logger</param>
        /// <param name="connect">connect</param>
        public SysService(ILogger<SysService> logger, IOptions<ConnectionStrings> connect)
        {
            try
            {
                _logger = logger;
                _connect = connect.Value;

                var client = new MongoClient(_connect.NoSqlContext);
                var database = client.GetDatabase(new MongoUrl(_connect.NoSqlContext).DatabaseName);
                _counters = database.GetCollection<counters>(typeof(counters).Name);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, string.Format("SysService -- {0}", ex.Message));
            }
        }

        /// <summary>
        /// Get increment
        /// </summary>
        /// <param name="sequence_name"></param>
        /// <returns></returns>
        public Int64 GetIncrement(string sequence_name)
        {
            try
            {
                var filter = Builders<counters>.Filter.Eq(s => s._id, sequence_name);
                var update = Builders<counters>.Update.Inc(s => s.seq, 1);
                var result = _counters.FindOneAndUpdate(filter, update, new FindOneAndUpdateOptions<counters, counters> { IsUpsert = true, ReturnDocument = ReturnDocument.After });
                return result.seq;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, string.Format("GetIncrement -- {0}", ex.Message));
                throw ex;
            }
        }
    }
}
