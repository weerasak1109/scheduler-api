﻿using Microsoft.Extensions.Localization;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using MongoDB.Driver;
using SSC.O2O.Scheduler.Entities.MongoDB;
using SSC.O2O.Scheduler.Helpers;
using SSC.O2O.Scheduler.Interfaces;
using SSC.O2O.Scheduler.Models;
using SSC.O2O.Scheduler.Resources;
using SSC.O2O.Scheduler.Utilities;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;

namespace SSC.O2O.Scheduler.Services
{
    /// <summary>
    /// Payment void service
    /// </summary>
    public class PayVoidService : ISchedulerService
    {
        private readonly ConnectionStrings _connect;
        private readonly IStringLocalizer _localizer;
        private readonly ILogger _logger;
        private readonly IMongoCollection<t_online_payment> _tonlinepayment;
        private readonly IMongoCollection<t_online_order> _tonlineorder;
        private readonly IMongoCollection<t_ct_qo> _tctqo;
        private readonly IMongoCollection<zz_e_order_status> _orderstatus;

        /// <summary>
        /// Payment void service constructor
        /// </summary>
        /// <param name="logger">logger</param>
        /// <param name="connect">connect</param>
        /// <param name="factory">factory</param>
        public PayVoidService(ILogger<PayVoidService> logger,
            IOptions<ConnectionStrings> connect,
            IStringLocalizerFactory factory)
        {
            try
            {
                _logger = logger;
                _connect = connect.Value;

                var type = typeof(ErrorMessage);
                var assemblyName = new AssemblyName(type.GetTypeInfo().Assembly.FullName);
                _localizer = factory.Create(typeof(ErrorMessage).Name, assemblyName.Name);

                var client = new MongoClient(_connect.NoSqlContext);
                var database = client.GetDatabase(new MongoUrl(_connect.NoSqlContext).DatabaseName);
                _tonlinepayment = database.GetCollection<t_online_payment>(typeof(t_online_payment).Name);
                _tonlineorder = database.GetCollection<t_online_order>(typeof(t_online_order).Name);
                _tctqo = database.GetCollection<t_ct_qo>(typeof(t_ct_qo).Name);
                _orderstatus = database.GetCollection<zz_e_order_status>(typeof(zz_e_order_status).Name);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, string.Format("{0} -- {1}", nameof(PayVoidService), ex.Message));
            }
        }


        /// <summary>
        /// ยกเลิกชำระะเงินออนไลน์
        /// </summary>
        /// <returns></returns>
        [DisplayName("Cancel payment")]
        public void Run()
        {
            try
            {
                #region Variable
                List<ReturnStockModels> add_stocks = new List<ReturnStockModels>();
                #endregion

                #region query online paymet
                DateTime s_date = DateTime.Now.ToLocalTime().AddDays(-2);
                var where1 = Builders<t_online_payment>.Filter.Eq(x => x.last_status.payment_status, ConstUtility.CREATE_PAYMENT);
                var where2 = Builders<t_online_payment>.Filter.Lte(x => x.edit_date, s_date);
                var where_all = Builders<t_online_payment>.Filter.And(where1, where2);

                List<t_online_payment> online_paymet = _tonlinepayment.Find(where_all).ToList();
                zz_e_order_status py_status = _orderstatus.Find(f => f.e_order_status_id == ConstUtility.CANCEL_ORDER).ToList().First();
                #endregion

                if (online_paymet != null && online_paymet.Count > 0 && py_status != null)
                {
                    foreach (var row in online_paymet)
                    {
                        add_stocks = new List<ReturnStockModels>();
                        var order = _tonlineorder.Find(f => f.o_pay_id == row.o_pay_id).ToList();
                        if (order != null && order.Count() != 0)
                        {
                            foreach (var col in order)
                            {
                                var builder_filter = Builders<t_online_order>.Filter;
                                var builder_update = Builders<t_online_order>.Update;
                                var filter = builder_filter.Eq(f => f.o_order_id, col.o_order_id);


                                var update = builder_update
                                        .Set(f => f.edit_by, 0)
                                        .Set(f => f.edit_date, DateTime.Now)
                                        .Set(f => f.last_status.order_status, py_status.e_order_status_id)
                                        .Set(f => f.last_status.buyer_order_status, py_status.purchase_status_name)
                                        .Set(f => f.last_status.seller_order_status, py_status.sale_status_name)
                                        .Set(f => f.last_status.platform_order_status, py_status.e_order_status_name)
                                        .Set(f => f.last_status.order_status_date, DateTime.Now);

                                var push = builder_update
                                    .Push(i => i.status_list, new t_online_order_status()
                                    {
                                        order_status = py_status.e_order_status_id,
                                        buyer_order_status = py_status.purchase_status_name,
                                        seller_order_status = py_status.sale_status_name,
                                        platform_order_status = py_status.e_order_status_name,
                                        order_status_date = DateTime.Now
                                    });


                                List<ReturnStockModels> add_stock = col.item_info.Select(s => new ReturnStockModels()
                                {
                                    qo_doc_id = s.qo_doc_id,
                                    item_id = s.item_id,
                                    qty = s.qty
                                }).ToList();

                                if (add_stock != null && add_stock.Count > 0) add_stocks.AddRange(add_stock);

                                _tonlineorder.UpdateOne(filter, update);
                                _tonlineorder.FindOneAndUpdate(filter, push);
                            }

                            if (add_stocks != null && add_stocks.Count > 0)
                            {
                                var b_filter = Builders<t_ct_qo>.Filter;
                                var b_update = Builders<t_ct_qo>.Update;

                                foreach (var stock in add_stocks)
                                {

                                    var qo = _tctqo.Find(f => f.doc_id == stock.qo_doc_id).First();
                                    if (qo != null) 
                                    {
                                        qo.items.on_hand += stock.qty;

                                        var filter = b_filter.Eq(f => f.doc_id, stock.qo_doc_id);

                                        var update = b_update
                                            .Set(f => f.items.on_hand, qo.items.on_hand);
                                        _tctqo.FindOneAndUpdate(filter, update);
                                    }
                                }
                            }                            
                        }

                        var pay_filter = Builders<t_online_payment>.Filter.Eq(f => f.o_pay_id, row.o_pay_id);
                        var pay_update = Builders<t_online_payment>.Update;

                        var pay_on_update = pay_update
                                .Set(f => f.edit_by, 0)
                                .Set(f => f.edit_date, DateTime.Now)
                                .Set(f => f.last_status.payment_status, py_status.e_order_status_id)
                                .Set(f => f.last_status.seller_payment_status, py_status.purchase_status_name)
                                .Set(f => f.last_status.platform_payment_status, py_status.sale_status_name)
                                .Set(f => f.last_status.buyer_payment_status, py_status.e_order_status_name)
                                .Set(f => f.last_status.payment_status_date, DateTime.Now);

                        var pay_push = pay_update
                            .Push(i => i.status_list, new t_online_payment_status()
                            {
                                payment_status = py_status.e_order_status_id,
                                seller_payment_status = py_status.purchase_status_name,
                                platform_payment_status = py_status.sale_status_name,
                                buyer_payment_status = py_status.e_order_status_name,
                                payment_status_date = DateTime.Now
                            });

                        _tonlinepayment.UpdateMany(pay_filter, pay_on_update);
                        _tonlinepayment.FindOneAndUpdate(pay_filter, pay_push);
                    }
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, string.Format("{0} -- {1}", nameof(PayVoidService), ex.Message));
            }
        }
    }
}
