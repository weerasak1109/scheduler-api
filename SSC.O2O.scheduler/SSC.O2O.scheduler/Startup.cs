﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using Hangfire;
using Hangfire.Dashboard;
using Hangfire.SqlServer;
using HangfireBasicAuthenticationFilter;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Localization;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Microsoft.OpenApi.Models;
using Serilog;
using Serilog.Events;
using Serilog.Sinks.Elasticsearch;
using SSC.O2O.Scheduler.Backgrounds;
using SSC.O2O.Scheduler.Helpers;
using SSC.O2O.Scheduler.Interfaces;
using SSC.O2O.Scheduler.Services;

namespace SSC.O2O.Scheduler
{
    /// <summary>
    /// Startup
    /// </summary>
    public class Startup
    {
        private string _product = "O2O Scheduler Services";
        private string _version = "1.0.0.0";
        private string _document = "SSC.O2O.Scheduler.xml";
        private string _route_prefix = String.Empty;

        /// <summary>
        /// Startup
        /// </summary>
        /// <param name="configuration"></param>
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;

            //Log.Logger = new LoggerConfiguration()
            //   .Enrich.FromLogContext()
            //   .MinimumLevel.Debug()
            //   .WriteTo.Elasticsearch(new ElasticsearchSinkOptions(new Uri(Configuration.GetSection("Elastic").GetSection("Url").Value))
            //   {
            //       MinimumLogEventLevel = LogEventLevel.Warning,
            //       AutoRegisterTemplate = true,
            //   })
            //   .CreateLogger();            
        }

        /// <summary>
        /// Configuration
        /// </summary>
        public IConfiguration Configuration { get; }

        /// <summary>
        /// This method gets called by the runtime. Use this method to add services to the container.
        /// </summary>
        /// <param name="services"></param>
        public void ConfigureServices(IServiceCollection services)
        {
            #region Version
            try
            {
                Assembly assemblys = typeof(Startup).GetTypeInfo().Assembly;
                _product = assemblys.GetCustomAttribute<AssemblyProductAttribute>().Product;
                _version = assemblys.GetCustomAttribute<AssemblyFileVersionAttribute>().Version;
                _document = $"{Assembly.GetExecutingAssembly().GetName().Name}.xml";
                _route_prefix = Configuration.GetSection("AppSettings").GetValue<String>("RoutePrefix");
            }
            catch
            {
                _product = "O2O Scheduler Services";
                _version = "1.0.0.0";
                _document = "SSC.O2O.Scheduler.xml";
                _route_prefix = String.Empty;

            }
            #endregion

            #region Configure app & connection setting
            services.Configure<AppSettings>(Configuration.GetSection("AppSettings"));
            services.Configure<ConnectionStrings>(Configuration.GetSection("ConnectionStrings"));
            services.Configure<RequestLocalizationOptions>(options =>
            {
                var supportedCultures = new[] { new CultureInfo("th-TH"), new CultureInfo("en-US") };
                options.DefaultRequestCulture = new RequestCulture("th-TH", "th-TH");
                options.SupportedCultures = supportedCultures;
                options.SupportedUICultures = supportedCultures;
            });
            #endregion

            services.AddMvc();
            services.AddRazorPages();
            services.AddControllers();
            services.AddControllersWithViews();
            
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc(_version, new OpenApiInfo { Title = _product, Version = _version });
                c.IncludeXmlComments(Path.Combine(AppContext.BaseDirectory, _document));
            });
            services.AddLocalization(o => o.ResourcesPath = "Resources");
            services.AddHangfire(configuration => configuration
                .SetDataCompatibilityLevel(CompatibilityLevel.Version_170)
                .UseSimpleAssemblyNameTypeSerializer()
                .UseRecommendedSerializerSettings()
                .UseSqlServerStorage(Configuration.GetConnectionString("MsSqlContext"), new SqlServerStorageOptions
                {
                    CommandBatchMaxTimeout = TimeSpan.FromMinutes(5),
                    SlidingInvisibilityTimeout = TimeSpan.FromMinutes(5),
                    QueuePollInterval = TimeSpan.Zero,
                    UseRecommendedIsolationLevel = true,
                    UsePageLocksOnDequeue = true,
                    DisableGlobalLocks = true
                }));
            services.AddHangfireServer();
            services.AddLogging(loggingBuilder => loggingBuilder.AddSerilog(dispose: true));

            #region Dependency injection
            services.AddScoped<ISysService, SysService>();
            services.AddScoped<ISchedulerService, PayVoidService>();
            services.AddScoped<ISchedulerService, OrderReportService>();
            #endregion
        }

        /// <summary>
        /// This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        /// </summary>
        /// <param name="app"></param>
        /// <param name="backgroundJobs"></param>
        /// <param name="env"></param>
        /// <param name="loggerFactory"></param>
        public void Configure(IApplicationBuilder app, IBackgroundJobClient backgroundJobs, IWebHostEnvironment env, ILoggerFactory loggerFactory)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }

            loggerFactory.AddSerilog();

            app.UseSwagger();
            app.UseRouting();
            app.UseCors("CorsPolicy");
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint(string.Format("{0}/swagger/{1}/swagger.json", _route_prefix, _version), string.Format("{0} {1}", _product, _version));
                c.InjectJavascript(string.Format("{0}/swagger-ui/custom.js", _route_prefix), "text/javascript");
                c.DocumentTitle = string.Format("{0} API", _product);
                c.RoutePrefix = "documents";

            });
            var options = app.ApplicationServices.GetService<IOptions<RequestLocalizationOptions>>();
            app.UseRequestLocalization(options.Value);
            app.UseStaticFiles();
            app.UseHttpsRedirection();
            app.UseHangfireServer();
            app.UseHangfireDashboard("/dashboard", new DashboardOptions()
            {
                IsReadOnlyFunc = (DashboardContext context) => true,
                DashboardTitle = _product,
                AppPath = _route_prefix == String.Empty? "/" : _route_prefix,
                Authorization = new[] {  // username สำหรับ login
                    new HangfireCustomBasicAuthenticationFilter { User = "admin-sn", Pass = "K2728123-S3n@t3-admin"}
                }
            });            
            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllerRoute(
                    name: "default",
                    pattern: "{controller=Home}/{action=Index}/{id?}");
                endpoints.MapControllers();
            });
            JobsBackgrounds.Run();
        }
    }
}