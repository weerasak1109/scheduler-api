﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SSC.O2O.Scheduler.Utilities
{
    /// <summary>
    /// ค่าคงที่
    /// </summary>
    public class ConstUtility
    {
        #region order status
        /// <summary>
        /// 
        /// </summary>
        public const Int64 CREATE_PAYMENT = 1;
        /// <summary>
        /// 
        /// </summary>
        public const Int64 CONFIRM_PAYMENT = 2;
        /// <summary>
        /// 
        /// </summary>
        public const Int64 CONFIRM_ORDER = 3;
        /// <summary>
        /// 
        /// </summary>
        public const Int64 TRANFER_ORDER = 4;
        /// <summary>
        /// 
        /// </summary>
        public const Int64 PARTIAL_DELIVERY = 5;
        /// <summary>
        /// 
        /// </summary>
        public const Int64 RECEIVE_ORDER = 50;
        /// <summary>
        /// 
        /// </summary>
        public const Int64 RECEIVE_ORDER_SHOP = 51;
        /// <summary>
        /// 
        /// </summary>
        public const Int64 CANCEL_ORDER = 100;
        /// <summary>
        /// 
        /// </summary>
        public const Int64 CONFIRM_PAY = 101;
        #endregion

        #region jobs type
        /// <summary>
        /// ยกเลิกคำสั่งซื้อ
        /// </summary>
        public const Int64 JOBS_VOID_ORDER = 1;
        /// <summary>
        /// สร้างรายงานคำสั้งซื้อ
        /// </summary>
        public const Int64 JOBS_REPORT_ORDER = 2;
        #endregion

        #region jobs action
        /// <summary>
        /// Recurring Hourly
        /// </summary>
        public const Int64 ACTION_JOBS_HOURLY = 1;
        /// <summary>
        /// Recurring Daily
        /// </summary>
        public const Int64 ACTION_JOBS_DAILY = 2;
        /// <summary>
        /// Recurring Weekly
        /// </summary>
        public const Int64 ACTION_JOBS_WEEKLY = 3;
        /// <summary>
        /// Recurring Monthly
        /// </summary>
        public const Int64 ACTION_JOBS_MONTHLY = 4;
        /// <summary>
        /// Recurring Yearly
        /// </summary>
        public const Int64 ACTION_JOBS_YEARLY = 5;
        #endregion

        #region payment calc type
        /// <summary>
        /// เปอร์เซ็นต์
        /// </summary>
        public const Int64 PY_CALC_PERCENT = 1;
        /// <summary>
        /// จำนวน
        /// </summary>
        public const Int64 PY_CALC_AMOUNT = 2;
        #endregion
    }
}

