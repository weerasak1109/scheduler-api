﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SSC.O2O.Scheduler.Interfaces
{
    /// <summary>
    /// Interface system service
    /// </summary>
    public interface ISysService
    {
        /// <summary>
        /// Get increment
        /// </summary>
        /// <param name="sequence_name"></param>
        /// <returns></returns>
        public Int64 GetIncrement(string sequence_name);
    }
}
