﻿using SSC.O2O.Scheduler.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SSC.O2O.Scheduler.Interfaces
{
    /// <summary>
    /// Interface scheduler service
    /// </summary>
    public interface ISchedulerService
    {
        /// <summary>
        /// Run
        /// </summary>
        /// <returns></returns>
        public void Run();
    }
}
