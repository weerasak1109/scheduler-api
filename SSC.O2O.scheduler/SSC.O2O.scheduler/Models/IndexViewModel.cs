﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SSC.O2O.Scheduler.Models
{
    /// <summary>
    /// Index View Model
    /// </summary>
    public class IndexViewModel
    {
        /// <summary>
        /// Route Prefix
        /// </summary>
        public string RoutePrefix { get; set; }
    }
}
