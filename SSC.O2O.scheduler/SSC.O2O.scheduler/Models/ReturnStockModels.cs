﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SSC.O2O.Scheduler.Models
{
    /// <summary>
    /// Return Stock
    /// </summary>
    public class ReturnStockModels
    {
        /// <summary>
        /// รหัสสินค้า
        /// </summary>
        public Int64 item_id { get; set; }
        /// <summary>
        /// รหัสโพสต์ขาย
        /// </summary>
        public Int64 qo_doc_id { get; set; }
        /// <summary>
        /// จำนวน
        /// </summary>
        public double qty { get; set; }
    }
}
