﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SSC.O2O.Scheduler.Models
{
    /// <summary>
    /// ข้อมูลผิดพลาด
    /// </summary>
    public class OutputModels<TValue> : OutputModels
    {        
        /// <summary>
        /// ข้อมูล
        /// </summary>
        public TValue data { get; set; }
    }
    /// <summary>
    /// ข้อมูลผิดพลาด
    /// </summary>
    public class OutputModels
    {
        /// <summary>
		/// รหัสข้อผิดพลาด
		/// </summary>
		public Int64 error_code { get; set; }
        /// <summary>
        /// รหัสย่อยข้อผิดพลาด
        /// </summary>
        public Int64 sub_code { get; set; }
        /// <summary>
        /// หัวเรื่อง
        /// </summary>
        public string title { get; set; }
        /// <summary>
        /// ข้อความ ข้อผิดพลาด
        /// </summary>
        public string message { get; set; }
    }
}
