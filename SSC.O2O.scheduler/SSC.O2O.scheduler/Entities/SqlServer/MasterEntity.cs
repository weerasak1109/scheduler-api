﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace SSC.O2O.Scheduler.Entities.SqlServer
{
    /// <summary>
    /// ควบคุมงาน
    /// </summary>
    public class m_jobs : BaseMasterEntity
    {
        /// <summary>
        /// รหัสควบคุมงาน
        /// </summary>
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Int64 jobs_id { get; set; }
        /// <summary>
        /// ประเภทงาน
        /// </summary>
        public Int64 jobs_type_id { get; set; }
        /// <summary>
        /// การทำงาน
        /// </summary>
        public Int64 jobs_action_id { get; set; }
        /// <summary>
        /// แบบเดือน
        /// </summary>
        public Int64 cron_month { get; set; }
        /// <summary>
        /// แบบสัปดาห์
        /// </summary>
        public Int64 cron_dayofweek { get; set; }
        /// <summary>
        /// แบบวัน
        /// </summary>
        public Int64 cron_day { get; set; }
        /// <summary>
        /// แบบชั่วโมง
        /// </summary>
        public Int64 cron_hour { get; set; }
        /// <summary>
        /// แบบนาที
        /// </summary>
        public Int64 cron_minute { get; set; }
    }
    /// <summary>
    /// คุณสมบัติของงาน
    /// </summary>
    public class m_jobs_property
    {
        /// <summary>
        /// รหัสควบคุมงาน
        /// </summary>
        public Int64 jobs_id { get; set; }
        /// <summary>
        /// ลำดับ
        /// </summary>
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Int64 seq { get; set; }
        /// <summary>
        /// รหัสคุณสมบัติของงาน
        /// </summary>
        public Int64 jobs_property_id { get; set; }       
        /// <summary>
        /// ชื่อคุณสมบัติของงาน
        /// </summary>
        public string jobs_property_name { get; set; }
    }
}
