﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SSC.O2O.Scheduler.Entities.SqlServer
{
    /// <summary>
    /// ข้อมูลคงที่
    /// </summary>
    public abstract class BaseConstEntity 
    {
        /// <summary>
        /// ไม่ใช้งาน
        /// </summary>
        public Boolean inactive { get; set; }
        /// <summary>
        /// ผู้สร้าง
        /// </summary>
        public Int64 create_by { get; set; }
        /// <summary>
        /// เวลาสร้าง
        /// </summary>
        public DateTime create_date { get; set; }
        /// <summary>
        /// สร้าง
        /// </summary>
        /// <param name="user_id">ผุ้สร้าง</param>
        public virtual void Create(Int64 user_id)
        {
            this.create_by = user_id;
            this.create_date = DateTime.Now;
            this.inactive = false;
        }
    }
    /// <summary>
    /// ข้อมูลตั้งค่า
    /// </summary>
    public abstract class BaseSettingEntity : BaseConstEntity
    {
        /// <summary>
        /// ผู้แก้ไข
        /// </summary>
        public Int64 edit_by { get; set; }
        /// <summary>
        /// เวลาแก้ไข
        /// </summary>
        public DateTime edit_date { get; set; }
        /// <summary>
        /// แก้ไข
        /// </summary>
        /// <param name="user_id">ผู้แก้ไข</param>
        public virtual void Edit(Int64 user_id)
        {
            this.edit_by = user_id;
            this.edit_date = DateTime.Now;
        }
    }
    /// <summary>
    /// ข้อมูลหลัก
    /// </summary>
    public abstract class BaseMasterEntity : BaseSettingEntity
    {
        /// <summary>
        /// รหัสสปอนเซอร์
        /// </summary>
        public Int64 wsp_id { get; set; }
    }
    /// <summary>
    /// ข้อมูลเอกสาร
    /// </summary>
    public abstract class DataDocumentEntity : BaseMasterEntity
    {
        /// <summary>
        /// รหัสเอกสาร
        /// </summary>
        public Int64 doc_id { get; set; }
        /// <summary>
        /// ประเภทเอกสาร
        /// </summary>
        public Int64 doc_type { get; set; }
        /// <summary>
        /// โค้ดเอกสาร
        /// </summary>
        public string doc_no { get; set; }
        /// <summary>
        /// ชื่อเอกสาร
        /// </summary>
        public string doc_name { get; set; }
        /// <summary>
        /// วันที่เอกสาร
        /// </summary>
        public DateTime doc_date { get; set; }
        /// <summary>
        /// รหัสผู้อนุมัติ
        /// </summary>
        public Int64? post_by { get; set; }
        /// <summary>
        /// วันที่อนุมัติ
        /// </summary>
        public DateTime? post_date { get; set; }
        /// <summary>
        /// อนุมัติ
        /// </summary>
        public bool approved { get; set; }
        /// <summary>
        /// สถานะยกเลิก
        /// </summary>
        public bool void_status { get; set; }
        /// <summary>
        /// เหตุผลยกเลิก
        /// </summary>
        public string void_reason { get; set; }
        /// <summary>
        /// รหัสผู้ยกเลิก
        /// </summary>
        public Int64? void_by { get; set; }
        /// <summary>
        /// วันที่ยกเลิก
        /// </summary>
        public DateTime? void_date { get; set; }
    }
    /// <summary>
    /// ข้อมูลรายการ
    /// </summary>
    public abstract class BaseTransactionEntity : BaseMasterEntity
    {
    }
}
