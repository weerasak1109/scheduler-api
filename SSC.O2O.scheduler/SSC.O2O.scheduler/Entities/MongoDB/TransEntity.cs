﻿using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SSC.O2O.Scheduler.Entities.MongoDB
{
    #region Payment 
    /// <summary>
    /// ชำระะเงินออนไลน์
    /// </summary>
    [BsonIgnoreExtraElements]
    public class t_online_payment : BaseTransactionEntity
    {
        /// <summary>
        /// รหัสชำระเงินออนไลน์
        /// </summary>
        public Int64 o_pay_id { get; set; }
        /// <summary>
        /// โค้ดชำระเงินออนไลน์
        /// </summary>
        public string o_pay_no { get; set; }
        /// <summary>
        /// เวลาชำระเงินออนไลน์
        /// </summary>
        public DateTime o_pay_date { get; set; }

        /// <summary>
        /// รหัสเอกสารช่องทางการขาย
        /// </summary>
        public Int64 sc_doc_id { get; set; }
        /// <summary>
        /// ประเภทเอกสารช่องทางการขาย
        /// </summary>
        public Int64 sc_doc_type { get; set; }
        /// <summary>
        /// ชื่อเอกสารช่องทางการขาย
        /// </summary>
        public string sc_doc_name { get; set; }

        /// <summary>
        /// รหัสร้านชื้อสินค้า
        /// </summary>
        public Int64 buyer_outlet_id { get; set; }
        /// <summary>
        /// โค้ดร้านชื้อสินค้า
        /// </summary>
        public string buyer_outlet_code { get; set; }
        /// <summary>
        /// ชื่อร้านชื้อสินค้า
        /// </summary>
        public string buyer_outlet_name { get; set; }
        /// <summary>
        /// รหัสผู้ชื้อสินค้า
        /// </summary>
        public Int64 buyer_user_id { get; set; }
        /// <summary>
        /// ผู้ชื้อสินค้า
        /// </summary>
        public string buyer_user_name { get; set; }

        /// <summary>
        /// จำนวนเงินทั้งหมด
        /// </summary>
        public double total_amt { get; set; }
        /// <summary>
        /// จำนวนเงินรวม
        /// </summary>
        public double order_total_amt { get; set; }
        /// <summary>
        /// ส่วนลดราคาสินค้า
        /// </summary>
        public double order_total_discount { get; set; }
        /// <summary>
        /// จำนวนเงินขนส่งสินค้ารวม
        /// </summary>
        public double ship_total_amt { get; set; }
        /// <summary>
        /// ส่วนลดค่าขนส่งรวม
        /// </summary>
        public double ship_total_discount { get; set; }
        /// <summary>
        /// อัตราค่าธรรมเนียมผู้ให้บริการชำระเงิน
        /// </summary>
        public double provider_fee_rate { get; set; }
        /// <summary>
        /// จำนวนเงินค่าธรรมเนียมผู้ให้บริการชำระเงิน
        /// </summary>
        public double provider_fee_amt { get; set; }
        /// <summary>
        /// อัตราค่าธรรมเนียมแพลตฟอร์มชำระเงิน %
        /// </summary>
        public double platform_fee_rate { get; set; }
        /// <summary>
        /// จำนวนเงินค่าธรรมเนียมแพลตฟอร์ม
        /// </summary>
        public double platform_fee_amt { get; set; }
        /// <summary>
        /// ส่วนต่างค่าธรรมเนียมแพลตฟอร์ม
        /// </summary>
        public double platform_fee_net { get; set; }
        /// <summary>
        /// รหัสประเภทอัตราค่าธรรมเนียมหรือค่าบริการ
        /// </summary>
        public Int64 py_calc_type { get; set; }
        /// <summary>
        /// ชื่อประเภทอัตราค่าธรรมเนียมหรือค่าบริการ
        /// </summary>
        public string py_calc_type_name { get; set; }

        /// <summary>
        /// รหัสบริการชำระเงิน
        /// </summary>
        public Int64 py_id { get; set; }
        /// <summary>
        /// ประเภทบริการชำระเงิน
        /// </summary>
        public Int64 py_type { get; set; }
        /// <summary>
        /// โค้ดบริการชำระเงิน
        /// </summary>
        public string py_code { get; set; }
        /// <summary>
        /// ชื้อบริการชำระเงิน
        /// </summary>
        public string py_name { get; set; }
        /// <summary>
        /// รหัสผู้ให้บริการชำระเงิน
        /// </summary>
        public Int64 py_outlet_id { get; set; }
        /// <summary>
        /// โค้ดผู้ให้บริการชำระเงิน
        /// </summary>
        public string py_outlet_code { get; set; }
        /// <summary>
        /// ชื่อผู้ให้บริการชำระเงิน
        /// </summary>
        public string py_outlet_name { get; set; }

        /// <summary>
        /// รหัสเอกสารสัญญาบริการชำระเงิน
        /// </summary>
        public Int64 ct_fee_doc_id { get; set; }
        /// <summary>
        /// ประเภทเอกสารสัญญาบริการชำระเงิน
        /// </summary>
        public Int64 ct_fee_doc_type { get; set; }
        /// <summary>
        /// โค้ดเอกสารสัญญาบริการชำระเงิน
        /// </summary>
        public string ct_fee_doc_no { get; set; }
        /// <summary>
        /// สถานะคืนเงิน
        /// </summary>
        public bool is_reconcile { get; set; }
        /// <summary>
        /// เลขที่อ้างอิงคำสั่งซื้อช่องทางการขาย
        /// </summary>
        public string ref_sc_no { get; set; }


        /// <summary>
        /// ข้อมูลชำระเงิน
        /// </summary>
        public t_online_payment_info info { get; set; }
        /// <summary>
        /// สถานะล่าสุด
        /// </summary>
        public t_online_payment_status last_status { get; set; }
        /// <summary>
        /// รายการสถานะ
        /// </summary>
        public List<t_online_payment_status> status_list { get; set; }

        /// <summary>
        /// กำหนดส่วนต่างค่าธรรมเนียม platform
        /// </summary>
        public void SetPlatformFeeNet()
        {
            this.platform_fee_net = this.platform_fee_amt - this.provider_fee_amt;
        }
    }
    /// <summary>
    /// สถานะชำระเงิน
    /// </summary>
    public class t_online_payment_status
    {
        /// <summary>
        /// รหัสสถานะชำระเงิน
        /// </summary>
        public Int64 payment_status { get; set; }
        /// <summary>
        /// สถานะชำระเงินสำหรับผู้ขาย
        /// </summary>
        public string seller_payment_status { get; set; }
        /// <summary>
        /// สถานะชำระเงินสำหรับ platform
        /// </summary>
        public string platform_payment_status { get; set; }
        /// <summary>
        /// สถานะชำระเงินสำหรับผู้ซื้อ
        /// </summary>
        public string buyer_payment_status { get; set; }
        /// <summary>
        /// เวลา
        /// </summary>
        public DateTime payment_status_date { get; set; }
    }
    /// <summary>
    /// ข้อมูลชำระเงิน
    /// </summary>
    public class t_online_payment_info
    {
        /// <summary>
        /// อ้างอิงรายการ
        /// </summary>
        public string transaction { get; set; }
        /// <summary>
        /// อ้างอิงบัตร
        /// </summary>
        public string card { get; set; }
        /// <summary>
        /// อ้างอิงหมายเลขใบแจ้งหนี้
        /// </summary>
        public string invoice_no { get; set; }
        /// <summary>
        /// อ้างอิง
        /// </summary>
        public string reference { get; set; }
        /// <summary>
        /// อ้างอิง1
        /// </summary>
        public string reference1 { get; set; }
        /// <summary>
        /// อ้างอิง2
        /// </summary>
        public string reference2 { get; set; }
        /// <summary>
        /// อ้างอิง2
        /// </summary>
        public string reference3 { get; set; }
        /// <summary>
        /// อ้างอิง4
        /// </summary>
        public string reference4 { get; set; }
        /// <summary>
        /// อ้างอิง5
        /// </summary>
        public string reference5 { get; set; }
        /// <summary>
        /// เวลา
        /// </summary>
        public DateTime last_date { get; set; }
    }
    #endregion

    #region Order
    /// <summary>
    /// บิลสั่งซื้อ
    /// </summary>
    [BsonIgnoreExtraElements]
    public class t_online_order : BaseTransactionEntity
    {
        /// <summary>
        /// รหัสบิล
        /// </summary>
        public Int64 o_order_id { get; set; }
        /// <summary>
        /// โค้ดบิล
        /// </summary>
        public string o_order_no { get; set; }
        /// <summary>
        /// เวลาบิล
        /// </summary>
        public DateTime o_order_date { get; set; }

        /// <summary>
        /// รหัสชำระเงินออนไลน์
        /// </summary>
        public Int64 o_pay_id { get; set; }
        /// <summary>
        /// โค้ดชำระเงินออนไลน์
        /// </summary>
        public string o_pay_no { get; set; }
        /// <summary>
        /// เวลาชำระเงินออนไลน์
        /// </summary>
        public DateTime o_pay_date { get; set; }
        /// <summary>
        /// รหัสบริการชำระเงิน
        /// </summary>
        public Int64 py_id { get; set; }
        /// <summary>
        /// โค้ดบริการชำระเงิน
        /// </summary>
        public string py_code { get; set; }
        /// <summary>
        /// ชื้อบริการชำระเงิน
        /// </summary>
        public string py_name { get; set; }

        /// <summary>
        /// รหัสเอกสารช่องทางการขาย
        /// </summary>
        public Int64 sc_doc_id { get; set; }
        /// <summary>
        /// ประเภทเอกสารช่องทางการขาย
        /// </summary>
        public Int64 sc_doc_type { get; set; }
        /// <summary>
        /// ชื่อเอกสารช่องทางการขาย
        /// </summary>
        public string sc_doc_name { get; set; }

        /// <summary>
        /// รหัสร้านขายสินค้า
        /// </summary>
        public Int64 seller_outlet_id { get; set; }
        /// <summary>
        /// โค้ดร้านขายสินค้า
        /// </summary>
        public string seller_outlet_code { get; set; }
        /// <summary>
        /// ชื่อร้านขายสินค้า
        /// </summary>
        public string seller_outlet_name { get; set; }

        /// <summary>
        /// รหัสร้านชื้อสินค้า
        /// </summary>
        public Int64 buyer_outlet_id { get; set; }
        /// <summary>
        /// โค้ดร้านชื้อสินค้า
        /// </summary>
        public string buyer_outlet_code { get; set; }
        /// <summary>
        /// ชื่อร้านชื้อสินค้า
        /// </summary>
        public string buyer_outlet_name { get; set; }
        /// <summary>
        /// รหัสผู้ชื้อสินค้า
        /// </summary>
        public Int64 buyer_user_id { get; set; }
        /// <summary>
        /// ผู้ชื้อสินค้า
        /// </summary>
        public string buyer_user_name { get; set; }

        /// <summary>
        /// รหัสเอกสารสัญญาค่าธรรมเนียม
        /// </summary>
        public Int64 ct_fee_doc_id { get; set; }
        /// <summary>
        /// ประเทภเอกสารสัญญาค่าธรรมเนียม
        /// </summary>
        public Int64 ct_fee_doc_type { get; set; }
        /// <summary>
        /// โค้ดเอกสารสัญญาค่าธรรมเนียม
        /// </summary>
        public string ct_fee_doc_no { get; set; }

        /// <summary>
        /// รวมค่าธรรมเนียมผู้ขาย
        /// </summary>
        public double sum_seller_fee_amt { get; set; }
        /// <summary>
        /// ค่าธรรมเนียม platform
        /// </summary>
        public double sum_platform_fee_amt { get; set; }
        /// <summary>
        /// รวมส่วนต่าง
        /// </summary>
        public double sum_platform_diff_amt { get; set; }
        /// <summary>
        /// รวมค่าธรรมเนียมสุทธิ
        /// </summary>
        public double sum_platform_fee_net { get; set; }
        /// <summary>
        /// รวมค่าธรรมเนียมผู้ซื้อ
        /// </summary>
        public double sum_buyer_fee_amt { get; set; }

        /// <summary>
        /// รวมค่าจัดส่ง
        /// </summary>
        public double sum_platform_shipping { get; set; }
        /// <summary>
        /// รวมค่าจัดส่งสำหรับผู้ให้บริการ
        /// </summary>
        public double sum_provider_shipping { get; set; }
        /// <summary>
        /// รวมส่วนต่างค่าจัดส่ง
        /// </summary>
        public double sum_platform_shipping_net { get; set; }

        /// <summary>
        /// ราคาสินค้ารวม
        /// </summary>
        public double sum_item_amt { get; set; }
        /// <summary>
        /// ภาษีรวม
        /// </summary>
        public double sum_for_vat { get; set; }
        /// <summary>
        /// ไม่ภาษีรวม
        /// </summary>
        public double sum_no_vat { get; set; }
        /// <summary>
        /// ภาษี
        /// </summary>
        public double vat_amt { get; set; }
        /// <summary>
        /// ส่วนลด
        /// </summary>
        public double discount_amt { get; set; }
        /// <summary>
        /// รวมทั้งหมด
        /// </summary>
        public double total_amt { get; set; }
        /// <summary>
        /// เลขที่อ้างอิงคำสั่งซื้อช่องทางการขาย
        /// </summary>
        public string ref_sc_no { get; set; }

        /// <summary>
        /// สถานะล่าสุด
        /// </summary>
        public t_online_order_status last_status { get; set; }
        /// <summary>
        /// ที่อยู่จัดส่งของ
        /// </summary>
        public t_online_order_address ship_info { get; set; }
        /// <summary>
        /// ที่อยู่ออกบิล
        /// </summary>
        public t_online_order_address bill_info { get; set; }
        /// <summary>
        /// รายการสินค้า
        /// </summary>
        public List<t_online_order_item> item_info { get; set; }
        /// <summary>
        /// บริการจัดส่ง
        /// </summary>
        public List<t_online_order_shipping> shipping_charge { get; set; }
        /// <summary>
        /// รายการสถานะ
        /// </summary>
        public List<t_online_order_status> status_list { get; set; }

        /// <summary>
        /// รวมค่าธรรมเนียมผู้ขาย
        /// </summary>
        public void SumSellerFeeAmt()
        {
            if (item_info != null) this.sum_seller_fee_amt = item_info.Sum(s => s.seller_fee_amt);
        }
        /// <summary>
        /// รวมค่าธรรมเนียม platform
        /// </summary>
        public void SumPlatformFeeAmt()
        {
            if (item_info != null) this.sum_platform_fee_amt = item_info.Sum(s => s.platform_fee_amt);
        }
        /// <summary>
        /// รวมค่าธรรมเนียม platform
        /// </summary>
        public void SumPlatformDiffAmt()
        {
            if (item_info != null) this.sum_platform_diff_amt = item_info.Sum(s => s.platform_diff_amt);
        }
        /// <summary>
        /// รวมค่าธรรมเนียมสุทธิ
        /// </summary>
        public void SumPlatformFeeNet()
        {
            if (item_info != null) this.sum_platform_fee_net = item_info.Sum(s => s.platform_fee_net);
        }
        /// <summary>
        /// รวมค่าธรรมเนียมผู้ซื้อ
        /// </summary>
        public void SumBuyerFeeAmt()
        {
            if (item_info != null) this.sum_buyer_fee_amt = item_info.Sum(s => s.buyer_fee_amt);
        }
        /// <summary>
        /// กำหนดราคารวมค่าจัดส่ง
        /// </summary>
        public void SumItemAmt()
        {
            if (item_info != null) this.sum_item_amt = item_info.Sum(s => s.price_show);
        }
        /// <summary>
        /// กำหนดราคารวม
        /// </summary>
        public void SumShippingCharge()
        {
            if (shipping_charge != null) this.sum_platform_shipping = shipping_charge.Sum(s => s.platform_price);
        }
        /// <summary>
        /// รวมค่าจัดส่งสำหรับผู้ให้บริการ
        /// </summary>
        public void SumShippingProvider()
        {
            if (shipping_charge != null) this.sum_provider_shipping = shipping_charge.Sum(s => s.provider_price);
        }
        /// <summary>
        /// รวมส่วนต่างค่าจัดส่ง
        /// </summary>
        public void SumShippingNet()
        {
            if (shipping_charge != null) this.sum_platform_shipping_net = shipping_charge.Sum(s => s.platform_net);
        }
        /// <summary>
        /// คำนวณราคารวมทั้งหมด
        /// </summary>
        public void SumTotal()
        {
            this.total_amt = this.sum_item_amt + this.sum_platform_shipping + this.vat_amt - this.discount_amt;
        }
    }
    /// <summary>
    /// ที่อยู่
    /// </summary>
    [BsonIgnoreExtraElements]
    public class t_online_order_address
    {
        /// <summary>
        /// ชื่อ
        /// </summary>
        public string first_name { get; set; }
        /// <summary>
        /// นามสกุล
        /// </summary>
        public string last_name { get; set; }
        /// <summary>
        /// ชื่อเต็ม
        /// </summary>
        public string name { get; set; }
        /// <summary>
        /// รหัสร้านค้า
        /// </summary>
        public Int64 shop_id { get; set; }
        /// <summary>
        /// ร้านค้า
        /// </summary>
        public string shop_name { get; set; }
        /// <summary>
        /// ที่อยู่
        /// </summary>
        public string address { get; set; }
        /// <summary>
        /// บ้านเลขที่ หมู่ หมู่บ้าน
        /// </summary>
        public string address_line1 { get; set; }
        /// <summary>
        /// ถนน ซอย แยกผู้รับ
        /// </summary>
        public string address_line2 { get; set; }
        /// <summary>
        /// รหัสตำบล/แขวง
        /// </summary>
        public Int64 district_id { get; set; }
        /// <summary>
        /// ตำบล/แขวง
        /// </summary>
        public string district_name { get; set; }
        /// <summary>
        /// รหัสเขต/อำเภอ
        /// </summary>
        public Int64 border_id { get; set; }
        /// <summary>
        /// เขต/อำเภอผู้รับ
        /// </summary>
        public string border_name { get; set; }
        /// <summary>
        /// รหัสจังหวัดผู้รับ
        /// </summary>
        public Int64 province_id { get; set; }
        /// <summary>
        /// จังหวัดผู้รับ
        /// </summary>
        public string province_name { get; set; }
        /// <summary>
        /// รหัสไปรษณีย์
        /// </summary>
        public string post_code { get; set; }
        /// <summary>
        /// เบอร์โทร
        /// </summary>
        public string tel { get; set; }
        /// <summary>
        /// เบอร์มือ
        /// </summary>
        public string mobile_no { get; set; }
        /// <summary>
        /// เลขประจำตัวผู้เสียภาษี
        /// </summary>
        public string tax_id { get; set; }
    }
    /// <summary>
    /// สินค้าที่ขาย
    /// </summary>
    [BsonIgnoreExtraElements]
    public class t_online_order_item
    {
        /// <summary>
        /// รหัสสินค้า
        /// </summary>
        public Int64 item_id { get; set; }
        /// <summary>
        /// รหัสโพสต์ขาย
        /// </summary>
        public Int64 qo_doc_id { get; set; }
        /// <summary>
        /// โค้ดสินค้า
        /// </summary>
        public string item_code { get; set; }
        /// <summary>
        /// บาร์โค้ดสินค้า
        /// </summary>
        public string barcode { get; set; }
        /// <summary>
        /// ชื่อสินค้า
        /// </summary>
        public string item_name { get; set; }
        /// <summary>
        /// รหัสรูปภาพสินค้า
        /// </summary>
        public Int64 content_id { get; set; }
        /// <summary>
        /// ที่อยู่รูปภาพสินค้า
        /// </summary>
        public string content_path { get; set; }
        /// <summary>
        /// ประเทภสินค้า
        /// </summary>
        public Int64 item_type { get; set; }
        /// <summary>
        /// ประเทภสั่งซื้อสินค้า
        /// </summary>
        public Int64 order_item_type { get; set; }

        /// <summary>
        /// รหัสหน่วย
        /// </summary>
        public Int64 unit_id { get; set; }
        /// <summary>
        /// ชื่อหน่วย
        /// </summary>
        public string unit_name { get; set; }
        /// <summary>
        /// เป็นหน่วยเล็ก
        /// </summary>
        public bool is_base_unit { get; set; }
        /// <summary>
        /// อัตราหน่วยเล็ก
        /// </summary>
        public double base_unit_rate_qty { get; set; }
        /// <summary>
        /// รหัสสินค้าหน่วยเล็ก
        /// </summary>
        public Int64 base_unit_item_id { get; set; }
        /// <summary>
        /// หน่วยจำนวนน้ำหนัก
        /// </summary>
        public Int64 weight_unit_id { get; set; }
        /// <summary>
        /// จำนวนน้ำหนักต่อหน่วย
        /// </summary>
        public double weight { get; set; }
        /// <summary>
        /// จำนวนน้ำหนักจริง
        /// </summary>
        public double weight_show { get; set; }

        /// <summary>
        /// อัตราค่าธรรมเนียมผู้ขาย
        /// </summary>
        public double seller_fee_rate { get; set; }
        /// <summary>
        /// ค่าธรรมเนียมผู้ขาย
        /// </summary>
        public double seller_fee_amt { get; set; }

        /// <summary>
        /// อัตราค่าธรรมเนียม platform
        /// </summary>
        public double platform_fee_rate { get; set; }
        /// <summary>
        /// ค่าธรรมเนียม platform
        /// </summary>
        public double platform_fee_amt { get; set; }
        /// <summary>
        /// ราคาสินค้าจากคำนวณ
        /// </summary>
        public double platform_calc_price { get; set; }
        /// <summary>
        /// ส่วนต่าง
        /// </summary>
        public double platform_diff_amt { get; set; }
        /// <summary>
        /// ค่าธรรมเนียมสุทธิ
        /// </summary>
        public double platform_fee_net { get; set; }

        /// <summary>
        /// อัตราค่าธรรมเนียมผู้ซื้อ
        /// </summary>
        public double buyer_fee_rate { get; set; }
        /// <summary>
        /// ค่าธรรมเนียมผู้ซื้อ
        /// </summary>
        public double buyer_fee_amt { get; set; }

        /// <summary>
        /// ต้นทุนต่อหน่วย
        /// </summary>
        public double base_cost { get; set; }
        /// <summary>
        /// ราคาต่อหน่วย
        /// </summary>
        public double base_price { get; set; }
        /// <summary>
        /// รหัสเมนู
        /// </summary>
        public Int64 menu_id { get; set; }
        /// <summary>
        /// โค้ดเมนู
        /// </summary>
        public string menu_code { get; set; }
        /// <summary>
        /// ชื่อเมนู
        /// </summary>
        public string menu_name { get; set; }
        /// <summary>
        /// ราคาเมนู
        /// </summary>
        public double menu_price { get; set; }
        /// <summary>
        /// จำนวน
        /// </summary>
        public double qty { get; set; }
        /// <summary>
        /// ราคาจากคำนวณ
        /// </summary>
        public double base_calc_amt { get; set; }
        /// <summary>
        /// ราคาขายจริง
        /// </summary>
        public double price_show { get; set; }
        /// <summary>
        /// ต้นทุนจริง
        /// </summary>
        public double cost_show { get; set; }

        /// <summary>
        /// รหัสติดตาม
        /// </summary>
        public Int64 o_track_id { get; set; }
        /// <summary>
        /// โค้ดติดตาม
        /// </summary>
        public string o_track_no { get; set; }
        /// <summary>
        /// เลขที่บรรจุ
        /// </summary>
        public string pack_no { get; set; }
        /// <summary>
        /// รวมค่าขนส่งกับราคาสินค้า
        /// </summary>
        public bool lg_include { get; set; }

        /// <summary>
        /// รหัสร้านโพสต์สินค้า
        /// </summary>
        public Int64 post_outlet_id { get; set; }
        /// <summary>
        /// โค้ดร้านโพสต์สินค้า
        /// </summary>
        public string post_outlet_code { get; set; }
        /// <summary>
        /// ชื่อร้านโพสต์สินค้า
        /// </summary>
        public string post_outlet_name { get; set; }

        /// <summary>
        /// กำหนดค่าธรรมเนียมสุทธิ
        /// </summary>
        public void SetPlatformFeeNet()
        {
            this.platform_fee_net = this.platform_fee_amt + this.platform_diff_amt;
        }
        /// <summary>
        /// กำหนดราคาขาย
        /// </summary>
        public void SetPriceShow()
        {
            this.price_show = this.menu_price * this.qty;
        }
        /// <summary>
        /// ต้นทุนต่อจริง
        /// </summary>
        public void SetCostShow()
        {
            this.cost_show = this.base_cost * this.qty;
        }
        /// <summary>
        /// กำหนดน้ำหนักจริง
        /// </summary>
        public void SetWeightShow()
        {
            this.weight_show = this.weight * this.qty;
        }
    }
    /// <summary>
    /// สถานะ
    /// </summary>
    [BsonIgnoreExtraElements]
    public class t_online_order_status
    {
        /// <summary>
        /// รหัสสถานะบิล
        /// </summary>
        public Int64 order_status { get; set; }
        /// <summary>
        /// สถานะบิลสำหรับผู้ขาย
        /// </summary>
        public string seller_order_status { get; set; }
        /// <summary>
        /// สถานะบิลสำหรับ platform
        /// </summary>
        public string platform_order_status { get; set; }
        /// <summary>
        /// สถานะบิลสำหรับผู้ซื้อ
        /// </summary>
        public string buyer_order_status { get; set; }
        /// <summary>
        /// เวลา
        /// </summary>
        public DateTime order_status_date { get; set; }
    }
    /// <summary>
    /// การส่งสินค้า
    /// </summary>
    [BsonIgnoreExtraElements]
    public class t_online_order_shipping
    {
        /// <summary>
        /// รหัสบริการส่งของ
        /// </summary>
        public Int64 service_id { get; set; }
        /// <summary>
        /// โค้ดบริการส่งของ
        /// </summary>
        public string service_code { get; set; }
        /// <summary>
        /// ชื่อบริการส่งของ
        /// </summary>
        public string service_name { get; set; }
        /// <summary>
        /// รหัสประเภทบริการส่งของ
        /// </summary>
        public Int64 service_type { get; set; }
        /// <summary>
        /// ชื่อประเภทบริการส่งของ
        /// </summary>
        public string service_type_name { get; set; }
        /// <summary>
        /// ราคา provider
        /// </summary>
        public double provider_price { get; set; }
        /// <summary>
        /// ราคา platform
        /// </summary>
        public double platform_price { get; set; }
        /// <summary>
        /// ส่วนต่งราคา platform
        /// </summary>
        public double platform_net { get; set; }
        /// <summary>
        /// จำนวนบรรจุ
        /// </summary>
        public double packing_qty { get; set; }
        /// <summary>
        /// จำนวนน้ำหนักรวม
        /// </summary>
        public double sum_weight { get; set; }
    }
    #endregion

    #region Tracking
    /// <summary>
    /// แพ็คสินค้า
    /// </summary>
    [BsonIgnoreExtraElements]
    public class t_online_tracking : BaseTransactionEntity
    {
        /// <summary>
        /// รหัสติดตาม
        /// </summary>
        public Int64 o_track_id { get; set; }
        /// <summary>
        /// โค้ดติดตาม
        /// </summary>
        public string o_track_no { get; set; }
        /// <summary>
        /// เวลาติดตาม
        /// </summary>
        public DateTime o_track_date { get; set; }
        /// <summary>
        /// เลขที่บรรจุ
        /// </summary>
        public string pack_no { get; set; }

        /// <summary>
        /// รหัสบิล
        /// </summary>
        public Int64 o_order_id { get; set; }
        /// <summary>
        /// โค้ดบิล
        /// </summary>
        public string o_order_no { get; set; }

        /// <summary>
        /// รหัสชำระเงินออนไลน์
        /// </summary>
        public Int64 o_pay_id { get; set; }
        /// <summary>
        /// โค้ดชำระเงินออนไลน์
        /// </summary>
        public string o_pay_no { get; set; }

        /// <summary>
        /// รหัสเอกสารช่องทางการขาย
        /// </summary>
        public Int64 sc_doc_id { get; set; }
        /// <summary>
        /// ประเภทเอกสารช่องทางการขาย
        /// </summary>
        public Int64 sc_doc_type { get; set; }
        /// <summary>
        /// ชื่อเอกสารช่องทางการขาย
        /// </summary>
        public string sc_doc_name { get; set; }

        /// <summary>
        /// รหัสร้านขายสินค้า
        /// </summary>
        public Int64 seller_outlet_id { get; set; }
        /// <summary>
        /// โค้ดร้านขายสินค้า
        /// </summary>
        public string seller_outlet_code { get; set; }
        /// <summary>
        /// ชื่อร้านขายสินค้า
        /// </summary>
        public string seller_outlet_name { get; set; }

        /// <summary>
        /// รหัสร้านชื้อสินค้า
        /// </summary>
        public Int64 buyer_outlet_id { get; set; }
        /// <summary>
        /// โค้ดร้านชื้อสินค้า
        /// </summary>
        public string buyer_outlet_code { get; set; }
        /// <summary>
        /// ชื่อร้านชื้อสินค้า
        /// </summary>
        public string buyer_outlet_name { get; set; }
        /// <summary>
        /// รหัสผู้ชื้อสินค้า
        /// </summary>
        public Int64 buyer_user_id { get; set; }
        /// <summary>
        /// ผู้ชื้อสินค้า
        /// </summary>
        public string buyer_user_name { get; set; }

        /// <summary>
        /// รหัสบริการส่งของ
        /// </summary>
        public Int64 service_id { get; set; }
        /// <summary>
        /// โค้ดบริการส่งของ
        /// </summary>
        public string service_code { get; set; }
        /// <summary>
        /// ชื่อบริการส่งของ
        /// </summary>
        public string service_name { get; set; }
        /// <summary>
        /// รหัสประเภทบริการส่งของ
        /// </summary>
        public Int64 service_type { get; set; }
        /// <summary>
        /// ชื่อประเภทบริการส่งของ
        /// </summary>
        public string service_type_name { get; set; }
        /// <summary>
        /// รหัสราคา
        /// </summary>
        public Int64 lg_rate_id { get; set; }
        /// <summary>
        /// ราคา provider
        /// </summary>
        public double provider_price { get; set; }
        /// <summary>
        /// ราคา platform
        /// </summary>
        public double platform_price { get; set; }
        /// <summary>
        /// ส่วนต่งราคา platform
        /// </summary>
        public double platform_net { get; set; }
        /// <summary>
        /// น้ำหนักรวม
        /// </summary>
        public double sum_weight { get; set; }

        /// <summary>
        /// รหัสราคาจริง
        /// </summary>
        public Int64 actual_lg_rate_id { get; set; }
        /// <summary>
        /// ราคา provider จริง
        /// </summary>
        public double actual_provider_price { get; set; }
        /// <summary>
        /// ราคา platfor จริง
        /// </summary>
        public double actual_platform_price { get; set; }
        /// <summary>
        /// ส่วนต่งราคา platform จริง
        /// </summary>
        public double actual_platform_net { get; set; }
        /// <summary>
        /// น้ำหนักจริงรวม
        /// </summary>
        public double actual_weight { get; set; }

        /// <summary>
        /// เลขที่อ้างอิงคำสั่งซื้อช่องทางการขาย
        /// </summary>
        public string ref_sc_no { get; set; }

        /// <summary>
        /// ที่อยู่ผู้ฝาก
        /// </summary>
        public t_online_tracking_address sender_address { get; set; }
        /// <summary>
        /// ที่อยู่ผู้รับ
        /// </summary>
        public t_online_tracking_address receive_address { get; set; }
        /// <summary>
        /// ที่อยู่ออกบิล
        /// </summary>
        public t_online_tracking_address billing_address { get; set; }
        /// <summary>
        /// สถานะการส่งข้อมูลหาผู้ใช้บริการขนส่ง
        /// </summary>
        public t_online_tracking_status provider_status { get; set; }
        /// <summary>
        /// แพ็คสินค้า
        /// </summary>
        public List<t_online_tracking_item> item_info { get; set; }
        /// <summary>
        /// ประวัติสถานะ
        /// </summary>
        public List<t_online_tracking_log> log_info { get; set; }
        /// <summary>
        /// สถานะการรับของ ของผู้ซื้อ
        /// </summary>
        public t_online_tracking_buyer_status buyer_status { get; set; }
        /// <summary>
        /// ประวัติ สถานะการรับของ ของผู้ซื้อ
        /// </summary>
        public List<t_online_tracking_buyer_status> buyer_status_list { get; set; }
    }
    /// <summary>
    /// ที่อยู่จัดส่งของแพ็คสินค้า
    /// </summary>
    [BsonIgnoreExtraElements]
    public class t_online_tracking_address : t_online_order_address
    {

    }
    /// <summary>
    /// สินค้าในแพ็ค
    /// </summary>
    [BsonIgnoreExtraElements]
    public class t_online_tracking_item
    {
        /// <summary>
        /// รหัสสินค้า
        /// </summary>
        public Int64 item_id { get; set; }
        /// <summary>
        /// รหัสโพสต์ขาย
        /// </summary>
        public Int64 qo_doc_id { get; set; }
        /// <summary>
        /// โค้ดสินค้า
        /// </summary>
        public string item_code { get; set; }
        /// <summary>
        /// บาร์โค้ดสินค้า
        /// </summary>
        public string barcode { get; set; }
        /// <summary>
        /// ชื่อสินค้า
        /// </summary>
        public string item_name { get; set; }
        /// <summary>
        /// ประเทภสินค้า
        /// </summary>
        public Int64 item_type { get; set; }
        /// <summary>
        /// รหัสรูปภาพสินค้า
        /// </summary>
        public Int64 content_id { get; set; }
        /// <summary>
        /// ที่อยู่รูปภาพสินค้า
        /// </summary>
        public string content_path { get; set; }
        /// <summary>
        /// รหัสหน่วย
        /// </summary>
        public Int64 unit_id { get; set; }
        /// <summary>
        /// ชื่อหน่วย
        /// </summary>
        public string unit_name { get; set; }
        /// <summary>
        /// เป็นหน่วยเล็ก
        /// </summary>
        public bool is_base_unit { get; set; }
        /// <summary>
        /// อัตราหน่วยเล็ก
        /// </summary>
        public double base_unit_rate_qty { get; set; }
        /// <summary>
        /// รหัสสินค้าหน่วยเล็ก
        /// </summary>
        public Int64 base_unit_item_id { get; set; }
        /// <summary>
        /// จำนวนน้ำหนัก
        /// </summary>
        public double weight { get; set; }
        /// <summary>
        /// จำนวนน้ำหนักจริง
        /// </summary>
        public double weight_show { get; set; }

        /// <summary>
        /// จำนวน
        /// </summary>
        public double qty { get; set; }
        /// <summary>
        /// ต้นทุนต่อหน่วย
        /// </summary>
        public double base_cost { get; set; }
        /// <summary>
        /// ราคาต่อหน่วย
        /// </summary>
        public double base_price { get; set; }
        /// <summary>
        /// ราคาเมนู
        /// </summary>
        public double menu_price { get; set; }
        /// <summary>
        /// ราคาขายจริง
        /// </summary>
        public double price_show { get; set; }
        /// <summary>
        /// ต้นทุนจริง
        /// </summary>
        public double cost_show { get; set; }

        /// <summary>
        /// รวมค่าขนส่งกับราคาสินค้า
        /// </summary>
        public bool lg_include { get; set; }

        /// <summary>
        /// รหัสร้านโพสต์สินค้า
        /// </summary>
        public Int64 post_outlet_id { get; set; }
        /// <summary>
        /// โค้ดร้านโพสต์สินค้า
        /// </summary>
        public string post_outlet_code { get; set; }
        /// <summary>
        /// ชื่อร้านโพสต์สินค้า
        /// </summary>
        public string post_outlet_name { get; set; }

    }
    /// <summary>
    /// ประวัติสถานะ
    /// </summary>
    [BsonIgnoreExtraElements]
    public class t_online_tracking_log
    {
        /// <summary>
        /// โค้ดประวัติสถานะ
        /// </summary>
        public string log_code { get; set; }
        /// <summary>
        /// ชื่อประวัติสถานะ
        /// </summary>
        public string log_name { get; set; }
        /// <summary>
        /// รายละเอียดประวัติสถานะ
        /// </summary>
        public string log_description { get; set; }
        /// <summary>
        /// สถานนี้ประวัติสถานะ
        /// </summary>
        public string log_station { get; set; }
        /// <summary>
        /// วันที่ลงประวัติสถานะ
        /// </summary>
        public DateTime log_date { get; set; }
    }
    /// <summary>
    /// สถานะการส่งข้อมูลหาผู้ใช้บริการขนส่ง
    /// </summary>
    [BsonIgnoreExtraElements]
    public class t_online_tracking_status
    {
        /// <summary>
        /// ส่งข้อมูลหาผู้ใช้บริการสำเร็จ
        /// </summary>
        public bool is_success { get; set; }
        /// <summary>
        /// โค้ดจากผู้ใช้บริการ
        /// </summary>
        public string res_code { get; set; }
        /// <summary>
        /// ข้อความผู้ใช้บริการ
        /// </summary>
        public string res_message { get; set; }
        /// <summary>
        /// หมายเลขใบรับฝาก
        /// </summary>
        public string res_manifest_no { get; set; }
        /// <summary>
        /// วันที่
        /// </summary>
        public DateTime res_date { get; set; }
    }
    /// <summary>
    /// สถานะการรับของ ของผู้ซื้อ
    /// </summary>
    [BsonIgnoreExtraElements]
    public class t_online_tracking_buyer_status
    {
        /// <summary>
        /// รหัสการควบคุมสถานะรับของ
        /// </summary>
        public Int64 buyer_event_id { get; set; }
        /// <summary>
        /// สถานะการรับของภาษาไทย
        /// </summary>
        public string buyer_event_name { get; set; }
        /// <summary>
        /// สถานะการรับของภาษาอังกฤษ
        /// </summary>
        public DateTime buyer_date { get; set; }
    }
    #endregion

    #region Menu Product
    /// <summary>
    /// ราคาขายออนไลน์
    /// </summary>
    [BsonIgnoreExtraElements]
    public class t_ct_qo : DataDocumentEntity
    {
        /// <summary>
        /// สินค้า
        /// </summary>
        [BsonIgnoreExtraElements]
        public class qo_item
        {
            /// <summary>
            /// รหัสสินค้า
            /// </summary>
            public Int64 item_id { get; set; }
            /// <summary>
            /// จำนวนคงเหลือ
            /// </summary>
            public double on_hand { get; set; }            
        }
        /// <summary>
        /// สินค้า
        /// </summary>
        public qo_item items { get; set; }
    }
    #endregion

    #region Online order report
    /// <summary>
    /// รายงานคำสั่งซื้อ
    /// </summary>
    public class t_online_order_report : BaseTransactionEntity
    {
        /// <summary>
        /// รหัสข้อมูลสำหรับรายงาน
        /// </summary>
        public Int64 o_report_id { get; set; }
        /// <summary>
        /// รหัสชำระเงินออนไลน์
        /// </summary>
        public Int64 o_pay_id { get; set; }
        /// <summary>
        /// โค้ดชำระเงินออนไลน์
        /// </summary>
        public string o_pay_no { get; set; }
        /// <summary>
        /// เวลาชำระเงินออนไลน์
        /// </summary>
        public DateTime o_pay_date { get; set; }
        /// <summary>
        /// รหัสบิล
        /// </summary>
        public Int64 o_order_id { get; set; }
        /// <summary>
        /// โค้ดบิล
        /// </summary>
        public string o_order_no { get; set; }
        /// <summary>
        /// เวลาบิล
        /// </summary>
        public DateTime o_order_date { get; set; }
        /// <summary>
        /// วันที่สั่งซื้อ
        /// </summary>
        public DateTime order_date { get; set; }
        /// <summary>
        /// หมายเลขอ้างอิงคำสั่งซื้อ
        /// </summary>
        public string ref_sc_no { get; set; }

        /// <summary>
        /// รหัสเอกสารช่องทางการขาย
        /// </summary>
        public Int64 sc_doc_id { get; set; }
        /// <summary>
        /// ประเภทเอกสารช่องทางการขาย
        /// </summary>
        public Int64 sc_doc_type { get; set; }
        /// <summary>
        /// ช่องทางการขาย
        /// </summary>
        public string sc_name { get; set; }
        
        /// <summary>
        /// หมายเลขจัดส่ง
        /// </summary>
        public string track_no { get; set; }
        /// <summary>
        /// รหัสติดตาม
        /// </summary>
        public Int64 o_track_id { get; set; }
        /// <summary>
        /// สถานะการจัดส่ง
        /// </summary>
        public string tracking_status { get; set; }

        /// <summary>
        /// ประเภทการชำระเงิน
        /// </summary>
        public string py_type { get; set; }
        /// <summary>
        /// รหัสบริการชำระเงิน
        /// </summary>
        public Int64 py_id { get; set; }        
        /// <summary>
        /// รหัสผู้ให้บริการชำระเงิน
        /// </summary>
        public Int64 py_outlet_id { get; set; }
        /// <summary>
        /// โค้ดผู้ให้บริการชำระเงิน
        /// </summary>
        public string py_outlet_code { get; set; }
        /// <summary>
        /// ชื่อผู้ให้บริการชำระเงิน
        /// </summary>
        public string py_outlet_name { get; set; }
        /// <summary>
        /// ลำดับสินค้า
        /// </summary>
        public Int64 order_item_seq { get; set; }
        /// <summary>
        /// รหัสสินค้า doc id
        /// </summary>
        public Int64 qo_doc_id { get; set; }
        /// <summary>
        /// รหัสสินค้า item id
        /// </summary>
        public Int64 item_id { get; set; }
        /// <summary>
        /// สินค้า
        /// </summary>
        public string item_name { get; set; }
        /// <summary>
        /// ต้นทุน / หน่วย
        /// </summary>
        public double cost_price { get; set; }
        /// <summary>
        /// จำนวน
        /// </summary>
        public Int64 qty { get; set; }
        /// <summary>
        /// ต้นทุนรวม
        /// </summary>
        public double total_cost { get; set; }
        /// <summary>
        /// ราคาขาย / หน่วย
        /// </summary>
        public double sale_price { get; set; }
        /// <summary>
        /// ราคาขายสินค้ารวม
        /// </summary>
        public double total_price { get; set; }

        /// <summary>
        /// ส่วนลดส่งเสริมการขาย
        /// </summary>
        public double disc_promotion { get; set; }
        /// <summary>
        /// ส่วนลดค่าจัดส่ง
        /// </summary>
        public double lg_disc { get; set; }
        /// <summary>
        /// ราคาสินค้า
        /// </summary>
        public double order_total_amt { get; set; }
        /// <summary>
        /// ราคาทั้งหมด
        /// </summary>
        public double total_amt { get; set; }
        /// <summary>
        /// ค่าธรรมเนียมบัตรเครดิต
        /// </summary>
        public double py_plateform_price { get; set; }
        /// <summary>
        /// ราคาสินค้ารวมค่าจัดส่ง
        /// </summary>
        public double order_wlg { get; set; }
        /// <summary>
        /// ต้นทุนค่าธรรมเนียม
        /// </summary>
        public double wov_provider_price { get; set; }
        /// <summary>
        /// ภาษีมูลค่าเพิ่ม(1)
        /// </summary>
        public double py_provider_price_vat { get; set; }
        /// <summary>
        /// รวมต้นทุนค่าธรรมเนียม
        /// </summary>
        public double py_provider_price { get; set; }
        /// <summary>
        /// ค่าธรรมเนียมบัตรเครดิต
        /// </summary>
        public double wov_plateform_price { get; set; }
        /// <summary>
        /// ภาษีมูลค่า เพิ่ม(2)
        /// </summary>
        public double py_plateform_price_vat { get; set; }
        /// <summary>
        /// ส่วนต่างค่าธรรมเนียม
        /// </summary>
        public double diff_py_fee { get; set; }
        /// <summary>
        /// ค่าจัดส่ง
        /// </summary>
        public double lg_price { get; set; }
        /// <summary>
        /// ต้นทุนค่าจัดส่ง
        /// </summary>
        public double wov_lg_provider_price { get; set; }
        /// <summary>
        /// ค่าจัดส่งภาษีมูลค่าเพิ่ม(1)
        /// </summary>
        public double lg_provider_price_vat { get; set; }
        /// <summary>
        /// รวมต้นทุนค่าจัดส่ง
        /// </summary>
        public double lg_provider_price { get; set; }
        /// <summary>
        /// ค่าจัดส่งเรียกเก็บจากผู้ซื้อ
        /// </summary>
        public double wov_lg_plateform_price { get; set; }
        /// <summary>
        /// ภาษีมูลค่าเพิ่ม(2)
        /// </summary>
        public double lg_plateform_price_vat { get; set; }
        /// <summary>
        /// รวมค่าจัดส่ง
        /// </summary>
        public double lg_plateform_price { get; set; }
        /// <summary>
        /// ส่วนต่างค่าจัดส่ง
        /// </summary>
        public double diff_lg_price { get; set; }
        /// <summary>
        /// ร้านค้า
        /// </summary>
        public string shop_name { get; set; }
        /// <summary>
        /// ผู้ประกอบการ
        /// </summary>
        public string seller_name { get; set; }
        /// <summary>
        /// เบอร์โทรศัพท์ผู้ประกอบการ
        /// </summary>
        public string seller_mobile_no { get; set; }

        /// <summary>
        /// Set Round
        /// </summary>
        public virtual void SetRound()
        {
            cost_price = Math.Round(cost_price, 4, MidpointRounding.AwayFromZero);
            total_cost = Math.Round(total_cost, 4, MidpointRounding.AwayFromZero);
            sale_price = Math.Round(sale_price, 4, MidpointRounding.AwayFromZero);
            total_price = Math.Round(total_price, 4, MidpointRounding.AwayFromZero);
            disc_promotion = Math.Round(disc_promotion, 4, MidpointRounding.AwayFromZero);
            lg_disc = Math.Round(lg_disc, 4, MidpointRounding.AwayFromZero);
            total_amt = Math.Round(total_amt, 4, MidpointRounding.AwayFromZero);
            py_plateform_price = Math.Round(py_plateform_price, 4, MidpointRounding.AwayFromZero);
            order_wlg = Math.Round(order_wlg, 4, MidpointRounding.AwayFromZero);
            wov_provider_price = Math.Round(wov_provider_price, 4, MidpointRounding.AwayFromZero);
            py_provider_price_vat = Math.Round(py_provider_price_vat, 4, MidpointRounding.AwayFromZero);
            py_provider_price = Math.Round(py_provider_price, 4, MidpointRounding.AwayFromZero);
            wov_plateform_price = Math.Round(wov_plateform_price, 4, MidpointRounding.AwayFromZero);
            py_plateform_price_vat = Math.Round(py_plateform_price_vat, 4, MidpointRounding.AwayFromZero);
            diff_py_fee = Math.Round(diff_py_fee, 4, MidpointRounding.AwayFromZero);
            lg_price = Math.Round(lg_price, 4, MidpointRounding.AwayFromZero);
            wov_lg_provider_price = Math.Round(wov_lg_provider_price, 4, MidpointRounding.AwayFromZero);
            lg_provider_price_vat = Math.Round(lg_provider_price_vat, 4, MidpointRounding.AwayFromZero);
            lg_provider_price = Math.Round(lg_provider_price, 4, MidpointRounding.AwayFromZero);
            wov_lg_plateform_price = Math.Round(wov_lg_plateform_price, 4, MidpointRounding.AwayFromZero);
            lg_plateform_price_vat = Math.Round(lg_plateform_price_vat, 4, MidpointRounding.AwayFromZero);
            lg_plateform_price = Math.Round(lg_plateform_price, 4, MidpointRounding.AwayFromZero);
            diff_lg_price = Math.Round(diff_lg_price, 4, MidpointRounding.AwayFromZero);
        }
    }
    #endregion
}
