﻿using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SSC.O2O.Scheduler.Entities.MongoDB
{
    #region Payment    
    /// <summary>
    /// บริการชำระเงินออนไลน์
    /// </summary>
    [BsonIgnoreExtraElements]
    public class m_payment : BaseMasterEntity
    {
        /// <summary>
        /// รหัสบริการชำระเงิน
        /// </summary>
        public Int64 py_id { get; set; }
        /// <summary>
        /// ประเภทบริการชำระเงิน
        /// </summary>
        public Int64 py_type { get; set; }
        /// <summary>
        /// โค้ดบริการชำระเงิน
        /// </summary>
        public string py_code { get; set; }
        /// <summary>
        /// ชื้อบริการชำระเงิน
        /// </summary>
        public string py_name { get; set; }
        /// <summary>
        /// ชื้อบริการชำระเงิน
        /// </summary>
        public string py_name1 { get; set; }
        /// <summary>
        /// ชื้อบริการชำระเงิน
        /// </summary>
        public string py_name2 { get; set; }
        /// <summary>
        /// ชื้อบริการชำระเงิน
        /// </summary>
        public string py_name3 { get; set; }
        /// <summary>
        /// ชื้อบริการชำระเงิน
        /// </summary>
        public string py_name4 { get; set; }
        /// <summary>
        /// ชื้อบริการชำระเงิน
        /// </summary>
        public string py_name5 { get; set; }
        /// <summary>
        /// รหัสรูปภาพโลโก้
        /// </summary>
        public Int64 content_id { get; set; }
        /// <summary>
        /// ที่อยู่รูปภาพโลโก้
        /// </summary>
        public string content_path { get; set; }

        /// <summary>
        /// รหัสผู้ให้บริการชำระเงิน
        /// </summary>
        public Int64 py_outlet_id { get; set; }
        /// <summary>
        /// โค้ดผู้ให้บริการชำระเงิน
        /// </summary>
        public string py_outlet_code { get; set; }
        /// <summary>
        /// ชื่อผู้ให้บริการชำระเงิน
        /// </summary>
        public string py_outlet_name { get; set; }
        /// <summary>
        /// อัตราค่าธรรมเนียม platform ชำระเงิน
        /// </summary>
        public double platform_fee_rate { get; set; }
        /// <summary>
        /// อัตราค่าธรรมเนียมผู้ให้บริการชำระเงิน %
        /// </summary>
        public double new_provider_fee_rate { get; set; }
        /// <summary>
        /// รหัสประเภทอัตราค่าธรรมเนียมหรือค่าบริการ
        /// </summary>
        public Int64 py_calc_type { get; set; }
        /// <summary>
        /// ชื่อประเภทอัตราค่าธรรมเนียมหรือค่าบริการ
        /// </summary>
        public string py_calc_type_name { get; set; }
        /// <summary>
        /// วันที่ใช้งาน
        /// </summary>
        public DateTime s_date { get; set; }
        /// <summary>
        /// วันหยุดใช้งาน
        /// </summary>
        public DateTime? e_date { get; set; }
        /// <summary>
        /// หมายเหตุ
        /// </summary>
        public string remark { get; set; }

        /// <summary>
        /// รหัสเอกสารหลัก
        /// </summary>
        public Int64 root_doc_id { get; set; }
        /// <summary>
        /// ประภทเอกสารหลัก
        /// </summary>
        public Int64 root_doc_type { get; set; }
        /// <summary>
        /// โค้ดเอกสารหลัก
        /// </summary>
        public string root_doc_no { get; set; }

        /// <summary>
        /// รหัสเอกสารแก้ไขล่าสุด
        /// </summary>
        public Int64 last_doc_id { get; set; }
        /// <summary>
        /// ประเภทเอกสารแก้ไขล่าสุด
        /// </summary>
        public Int64 last_doc_type { get; set; }
        /// <summary>
        /// โค้ดเอกสารแก้ไขล่าสุด
        /// </summary>
        public string last_doc_no { get; set; }

        /// <summary>
        /// ไม่รู้
        /// </summary>
        public string py_value { get; set; }
        /// <summary>
        /// ไม่รู้
        /// </summary>
        public bool have_change { get; set; }
        /// <summary>
        /// ไม่รู้
        /// </summary>
        public bool open_drawer { get; set; }
        /// <summary>
        /// ไม่รู้
        /// </summary>
        public string pic_path { get; set; }
        /// <summary>
        /// ไม่รู้
        /// </summary>
        public string pic_path_local { get; set; }
        /// <summary>
        /// ไม่รู้
        /// </summary>
        public bool on_pos { get; set; }
        /// <summary>
        /// ไม่รู้
        /// </summary>
        public bool on_ecom { get; set; }
        /// <summary>
        /// ไม่รู้
        /// </summary>
        public long sort_by { get; set; }
    }
    #endregion
}
