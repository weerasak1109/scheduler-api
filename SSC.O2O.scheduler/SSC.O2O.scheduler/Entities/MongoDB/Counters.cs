﻿using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SSC.O2O.Scheduler.Entities.MongoDB
{
    /// <summary>
    /// counters
    /// </summary>
    [BsonIgnoreExtraElements]
    public class counters
    {
        /// <summary>
        /// _id
        /// </summary>
        public string _id { get; set; }
        /// <summary>
        /// seq
        /// </summary>
        public Int64 seq { get; set; }
    }
}
