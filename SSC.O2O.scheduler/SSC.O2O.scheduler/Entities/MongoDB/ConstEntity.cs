﻿using MongoDB.Bson.Serialization.Attributes;
using SSC.O2O.Scheduler.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SSC.O2O.Scheduler.Entities.MongoDB
{
    /// <summary>
    /// สถานะคำสั่งซื้อ
    /// </summary>
    [BsonIgnoreExtraElements]
    public class zz_e_order_status : BaseConstEntity
    {
        /// <summary>
        /// รหัสสถานะ
        /// </summary>
        public Int64 e_order_status_id { get; set; }
        /// <summary>
        /// สถานะ
        /// </summary>
        public string e_order_status_name { get; set; }
        /// <summary>
        /// สถานะ
        /// </summary>
        public string purchase_status_name { get; set; }
        /// <summary>
        /// สถานะ
        /// </summary>
        public string sale_status_name { get; set; }        
    }
    /// <summary>
    /// ประเภทงาน
    /// </summary>
    [BsonIgnoreExtraElements]
    public class zz_jobs_type : BaseConstEntity
    {
        /// <summary>
        /// Default
        /// </summary>
        /// <returns></returns>
        public static List<zz_jobs_type> GetDefault()
        {
            return new List<zz_jobs_type>() {
                new zz_jobs_type() { jobs_type_id = ConstUtility.JOBS_VOID_ORDER,jobs_type_name  = "Cancel an order",jobs_tag_name="Cancel an order"},
                new zz_jobs_type() { jobs_type_id = ConstUtility.JOBS_REPORT_ORDER,jobs_type_name  = "Create report data order",jobs_tag_name="Create report data order"}
            };
        }

        /// <summary>
        /// รหัสประเภทงาน
        /// </summary>
        public Int64 jobs_type_id { get; set; }
        /// <summary>
        /// ชื่อประเภทงาน
        /// </summary>
        public string jobs_type_name { get; set; }
        /// <summary>
        /// ชื่อใน service
        /// </summary>
        public string jobs_tag_name { get; set; }
    }
    /// <summary>
    /// คุณสมบัติของงาน
    /// </summary>
    public class zz_jobs_property
    {
        /// <summary>
        /// รหัสคุณสมบัติของงาน
        /// </summary>
        public Int64 jobs_property_id { get; set; }
        /// <summary>
        /// รหัสประเภทงาน
        /// </summary>
        public Int64 jobs_type_id { get; set; }
        /// <summary>
        /// รหัสประเภทข้อมูล
        /// </summary>
        public Int64 data_type_id { get; set; }
        /// <summary>
        /// ชื่อคุณสมบัติของงาน
        /// </summary>
        public string jobs_property_name { get; set; }
    }
    /// <summary>
    /// การทำงาน
    /// </summary>
    [BsonIgnoreExtraElements]
    public class zz_jobs_action : BaseConstEntity
    {
        /// <summary>
        /// Default
        /// </summary>
        /// <returns></returns>
        public static List<zz_jobs_action> GetDefault()
        {
            return new List<zz_jobs_action>() {
                new zz_jobs_action() { jobs_action_id = ConstUtility.ACTION_JOBS_HOURLY,jobs_action_name  = "Recurring Hourly"},
                new zz_jobs_action() { jobs_action_id = ConstUtility.ACTION_JOBS_DAILY,jobs_action_name  = "Recurring Daily"},
                new zz_jobs_action() { jobs_action_id = ConstUtility.ACTION_JOBS_WEEKLY,jobs_action_name  = "Recurring Weekly"},
                new zz_jobs_action() { jobs_action_id = ConstUtility.ACTION_JOBS_MONTHLY,jobs_action_name  = "Recurring Monthly"},
                new zz_jobs_action() { jobs_action_id = ConstUtility.ACTION_JOBS_YEARLY,jobs_action_name  = "Recurring Yearly"}
            };
        }

        /// <summary>
        /// รหัสการทำงาน
        /// </summary>
        public Int64 jobs_action_id { get; set; }
        /// <summary>
        /// ชื่อการทำงาน
        /// </summary>
        public string jobs_action_name { get; set; }
    }
    /// <summary>
    /// ประเภทรายงาน
    /// </summary>
    public class zz_online_report : BaseConstEntity
    {
        /// <summary>
        /// รหัสประเภทรายงาน
        /// </summary>
        public Int64 o_report_type { get; set; }
        /// <summary>
        /// ชื่อประเภทรายงาน
        /// </summary>
        public string o_report_type_name { get; set; }
    }
}
